%define name 	gds-swig
%define version 2.19.4
%define release 1.1
%define daswg   /usr
%define prefix  %{daswg}
%define _sysconfdir /etc
%define config_runflag --sysconfdir=%{_sysconfdir}
%define config_pygds --enable-python
%define gds_python_version 3.6

%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

Name: 		%{name}
Summary: 	gds-swig 2.19.4
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems SWIG bindings
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc
BuildRequires:  automake autoconf libtool m4 make
BuildRequires:  gzip bzip2 libXpm-devel
BuildRequires:  python3 python3-devel python36-numpy
BuildRequires:  python python-devel python2-numpy
BuildRequires:  readline-devel fftw-devel swig
BuildRequires:  jsoncpp-devel
BuildRequires:  gds-base-devel >= 2.19.5
BuildRequires:  gds-lsmp-devel >= 2.19.7
BuildRequires:  gds-frameio-devel >= 2.19.5
Requires:       gds-base >= 2.19.5
Requires:       gds-lsmp >= 2.19.7
Requires:       gds-frameio-base >= 2.19.5
Prefix:		      %prefix

%description
Global diagnostics software SWIG bindings

%package -n gds-pygds
Summary: 	Python wrapper for gds classes
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name} = %{version}-%{release}
Requires: gds-frameio-base >= 2.19.5
Requires: gds-lsmp >= 2.19.7

%description -n gds-pygds
Python wrappers of some of the most useful GDS classes.

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  \
    PYTHON=/usr/bin/python%{gds_python_version} \
    PYTHON_VERSION=%{gds_python_version} \
    --prefix=%prefix --libdir=%{_libdir} \
	  --includedir=%{prefix}/include/%{name} \
	  %{config_pygds} %{config_runflag}
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files

%files -n gds-pygds
%{python3_sitearch}/gds/*

%changelog
* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Updated for release as described in NEWS.md

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Split lowlatency as a separate package
